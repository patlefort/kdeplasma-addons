# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
# Wantoyo <wantoyek@gmail.com>, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-06-16 00:49+0000\n"
"PO-Revision-Date: 2018-10-10 07:17+0700\n"
"Last-Translator: Wantoyo <wantoyek@gmail.com>\n"
"Language-Team: Indonesian <kde-i18n-doc@kde.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: katesessions.cpp:30
#, kde-format
msgid "Finds Kate sessions matching :q:."
msgstr "Temukan sesi Kate yang cocok :q:."

#: katesessions.cpp:31
#, kde-format
msgid "Lists all the Kate editor sessions in your account."
msgstr "Daftar semua sesi editor Kate di dalam akunmu."

#: katesessions.cpp:67 katesessions.cpp:78
#, kde-format
msgid "Open Kate Session"
msgstr "Buka Sesi Kate"
