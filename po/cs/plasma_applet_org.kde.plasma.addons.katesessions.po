# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Vít Pelčák <vit@pelcak.org>, 2010, 2014.
# Vit Pelcak <vpelcak@suse.cz>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_katesession\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-17 00:48+0000\n"
"PO-Revision-Date: 2021-07-02 18:21+0200\n"
"Last-Translator: Vit Pelcak <vpelcak@suse.cz>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 21.04.2\n"

#: contents/ui/KateSessionsItemDelegate.qml:91
#, kde-format
msgid "Session name"
msgstr "Název sezení"

#: contents/ui/KateSessionsItemDelegate.qml:102
#, kde-format
msgid "Create new session and start Kate"
msgstr "Vytvořit nové sezení a spustit Kate"

#: contents/ui/KateSessionsItemDelegate.qml:111
#, kde-format
msgid "Cancel session creation"
msgstr "Zrušit vytváření sezení"

#: contents/ui/main.qml:29
#, kde-format
msgid "Kate Sessions"
msgstr "Sezení Kate"

#: contents/ui/main.qml:38
#, kde-format
msgid "Search…"
msgstr "Hledat…"

#~ msgid "Delete session"
#~ msgstr "Smazat sezení"

#~ msgid "Start Kate (no arguments)"
#~ msgstr "Spustit Kate (bez argumentů)"

#~ msgid "New Kate Session"
#~ msgstr "Nové sezení Kate"
