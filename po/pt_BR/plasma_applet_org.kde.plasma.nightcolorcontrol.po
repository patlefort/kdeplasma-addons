# Translation of plasma_applet_org.kde.plasma.nightcolorcontrol.po to Brazilian Portuguese
# Copyright (C) 2020 This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
#
# André Marcelo Alvarenga <alvarenga@kde.org>, 2020.
# Luiz Fernando Ranghetti <elchevive@opensuse.org>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-12-13 00:44+0000\n"
"PO-Revision-Date: 2021-12-21 09:27-0300\n"
"Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>\n"
"Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 20.04.2\n"

#: package/contents/ui/main.qml:31
#, kde-format
msgid "Night Color Control"
msgstr "Controle da cor noturna"

#: package/contents/ui/main.qml:34
#, kde-format
msgid "Night Color is inhibited"
msgstr "A cor noturna está inibida"

#: package/contents/ui/main.qml:37
#, kde-format
msgid "Night Color is unavailable"
msgstr "A cor noturna está indisponível"

#: package/contents/ui/main.qml:40
#, kde-format
msgid "Night Color is disabled"
msgstr "A cor noturna está desativada"

#: package/contents/ui/main.qml:43
#, kde-format
msgid "Night Color is not running"
msgstr "A cor noturna não está em execução"

#: package/contents/ui/main.qml:45
#, kde-format
msgid "Night Color is active (%1K)"
msgstr "A cor noturna está ativada (%1K)"

#: package/contents/ui/main.qml:94
#, kde-format
msgid "Configure Night Color…"
msgstr "Configurar a cor noturna..."
