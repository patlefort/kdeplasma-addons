# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
#
# Matjaž Jeran <matjaz.jeran@amis.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-05-30 00:46+0000\n"
"PO-Revision-Date: 2022-10-21 08:21+0200\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"X-Generator: Poedit 3.1.1\n"

#: calendarsystem.h:53
#, kde-format
msgctxt "@item:inlist"
msgid "Julian"
msgstr "Julijanski"

#: calendarsystem.h:57
#, kde-format
msgctxt "@item:inlist"
msgid "Milankovic"
msgstr "Milankovićev"

#: calendarsystem.h:61
#, kde-format
msgctxt "@item:inlist"
msgid "The Solar Hijri Calendar (Persian)"
msgstr "Sončni Hijri koledar (Perzijski)"

#: calendarsystem.h:65
#, kde-format
msgctxt "@item:inlist"
msgid "The Islamic Civil Calendar"
msgstr "Islamski civilni koledar"

#: calendarsystem.h:69
#, kde-format
msgctxt "@item:inlist"
msgid "Chinese Lunar Calendar"
msgstr "Kitajski lunin koledar"

#: calendarsystem.h:73
#, kde-format
msgctxt "@item:inlist"
msgid "Indian National Calendar"
msgstr "Indijski nacionalni koledar"

#: config/qml/AlternateCalendarConfig.qml:38
#, kde-format
msgctxt "@label:listbox"
msgid "Calendar system:"
msgstr "Koledarski sistem:"

#: config/qml/AlternateCalendarConfig.qml:50
#, kde-format
msgctxt "@label:spinbox"
msgid "Date offset:"
msgstr "Odmik datuma:"

#: config/qml/AlternateCalendarConfig.qml:59
#, kde-format
msgid "%1 day"
msgid_plural "%1 days"
msgstr[0] "%1 dan"
msgstr[1] "%1 dneva"
msgstr[2] "%1 dni"
msgstr[3] "%1 dni"
