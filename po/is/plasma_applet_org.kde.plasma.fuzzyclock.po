# translation of plasma_applet_fuzzy_clock.po to
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Sveinn í Felli <sveinki@nett.is>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_fuzzy_clock\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-01 00:45+0000\n"
"PO-Revision-Date: 2011-03-24 09:47+0000\n"
"Last-Translator: \n"
"Language-Team:  <en@li.org>\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;\n"
"\n"
"\n"

#: package/contents/config/config.qml:13
#, fuzzy, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr "Útlit"

#: package/contents/ui/configAppearance.qml:24
#, fuzzy, kde-format
msgctxt "@title:group"
msgid "Font:"
msgstr "Leturstíll:"

#: package/contents/ui/configAppearance.qml:25
#, fuzzy, kde-format
msgctxt "@option:check"
msgid "Bold text"
msgstr "&Skáletur"

#: package/contents/ui/configAppearance.qml:30
#, fuzzy, kde-format
msgctxt "@option:check"
msgid "Italic text"
msgstr "&Skáletur"

#: package/contents/ui/configAppearance.qml:39
#, fuzzy, kde-format
msgctxt "@title:group"
msgid "Fuzzyness:"
msgstr "Hversu mikið loðið:"

#: package/contents/ui/configAppearance.qml:49
#, kde-format
msgctxt "@item:inrange"
msgid "Accurate"
msgstr ""

#: package/contents/ui/configAppearance.qml:57
#, kde-format
msgctxt "@item:inrange"
msgid "Fuzzy"
msgstr ""

#: package/contents/ui/FuzzyClock.qml:34
#, fuzzy, kde-format
msgid "One o’clock"
msgstr "á slaginu %1"

#: package/contents/ui/FuzzyClock.qml:35
#, fuzzy, kde-format
msgid "Five past one"
msgstr "fimm mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:36
#, fuzzy, kde-format
msgid "Ten past one"
msgstr "tíu mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:37
#, fuzzy, kde-format
msgid "Quarter past one"
msgstr "korter yfir %1"

#: package/contents/ui/FuzzyClock.qml:38
#, fuzzy, kde-format
msgid "Twenty past one"
msgstr "tuttugu mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:39
#, fuzzy, kde-format
msgid "Twenty-five past one"
msgstr "tuttuguogfimm mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:40
#, fuzzy, kde-format
msgid "Half past one"
msgstr "%1 þrjátíu"

#: package/contents/ui/FuzzyClock.qml:41
#, fuzzy, kde-format
msgid "Twenty-five to two"
msgstr "tuttuguogfimm mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:42
#, fuzzy, kde-format
msgid "Twenty to two"
msgstr "tuttugu mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:43
#, fuzzy, kde-format
msgid "Quarter to two"
msgstr "korter í %1"

#: package/contents/ui/FuzzyClock.qml:44
#, fuzzy, kde-format
msgid "Ten to two"
msgstr "tíu mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:45
#, fuzzy, kde-format
msgid "Five to two"
msgstr "fimm mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:46
#, fuzzy, kde-format
msgid "Two o’clock"
msgstr "á slaginu %1"

#: package/contents/ui/FuzzyClock.qml:47
#, fuzzy, kde-format
msgid "Five past two"
msgstr "fimm mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:48
#, fuzzy, kde-format
msgid "Ten past two"
msgstr "tíu mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:49
#, fuzzy, kde-format
msgid "Quarter past two"
msgstr "korter yfir %1"

#: package/contents/ui/FuzzyClock.qml:50
#, fuzzy, kde-format
msgid "Twenty past two"
msgstr "tuttugu mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:51
#, fuzzy, kde-format
msgid "Twenty-five past two"
msgstr "tuttuguogfimm mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:52
#, fuzzy, kde-format
msgid "Half past two"
msgstr "%1 þrjátíu"

#: package/contents/ui/FuzzyClock.qml:53
#, fuzzy, kde-format
msgid "Twenty-five to three"
msgstr "tuttuguogfimm mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:54
#, fuzzy, kde-format
msgid "Twenty to three"
msgstr "tuttugu mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:55
#, fuzzy, kde-format
msgid "Quarter to three"
msgstr "korter í %1"

#: package/contents/ui/FuzzyClock.qml:56
#, kde-format
msgid "Ten to three"
msgstr ""

#: package/contents/ui/FuzzyClock.qml:57
#, fuzzy, kde-format
msgid "Five to three"
msgstr "fimm mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:58
#, kde-format
msgid "Three o’clock"
msgstr ""

#: package/contents/ui/FuzzyClock.qml:59
#, fuzzy, kde-format
msgid "Five past three"
msgstr "fimm mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:60
#, fuzzy, kde-format
msgid "Ten past three"
msgstr "tíu mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:61
#, fuzzy, kde-format
msgid "Quarter past three"
msgstr "korter yfir %1"

#: package/contents/ui/FuzzyClock.qml:62
#, fuzzy, kde-format
msgid "Twenty past three"
msgstr "tuttugu mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:63
#, fuzzy, kde-format
msgid "Twenty-five past three"
msgstr "tuttuguogfimm mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:64
#, fuzzy, kde-format
msgid "Half past three"
msgstr "%1 þrjátíu"

#: package/contents/ui/FuzzyClock.qml:65
#, fuzzy, kde-format
msgid "Twenty-five to four"
msgstr "tuttuguogfimm mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:66
#, fuzzy, kde-format
msgid "Twenty to four"
msgstr "tuttugu mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:67
#, fuzzy, kde-format
msgid "Quarter to four"
msgstr "korter í %1"

#: package/contents/ui/FuzzyClock.qml:68
#, kde-format
msgid "Ten to four"
msgstr ""

#: package/contents/ui/FuzzyClock.qml:69
#, fuzzy, kde-format
msgid "Five to four"
msgstr "fimm mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:70
#, kde-format
msgid "Four o’clock"
msgstr ""

#: package/contents/ui/FuzzyClock.qml:71
#, fuzzy, kde-format
msgid "Five past four"
msgstr "fimm mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:72
#, fuzzy, kde-format
msgid "Ten past four"
msgstr "tíu mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:73
#, fuzzy, kde-format
msgid "Quarter past four"
msgstr "korter yfir %1"

#: package/contents/ui/FuzzyClock.qml:74
#, fuzzy, kde-format
msgid "Twenty past four"
msgstr "tuttugu mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:75
#, fuzzy, kde-format
msgid "Twenty-five past four"
msgstr "tuttuguogfimm mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:76
#, fuzzy, kde-format
msgid "Half past four"
msgstr "%1 þrjátíu"

#: package/contents/ui/FuzzyClock.qml:77
#, fuzzy, kde-format
msgid "Twenty-five to five"
msgstr "tuttuguogfimm mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:78
#, fuzzy, kde-format
msgid "Twenty to five"
msgstr "tuttugu mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:79
#, fuzzy, kde-format
msgid "Quarter to five"
msgstr "korter í %1"

#: package/contents/ui/FuzzyClock.qml:80
#, kde-format
msgid "Ten to five"
msgstr ""

#: package/contents/ui/FuzzyClock.qml:81
#, fuzzy, kde-format
msgid "Five to five"
msgstr "fimm mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:82
#, kde-format
msgid "Five o’clock"
msgstr ""

#: package/contents/ui/FuzzyClock.qml:83
#, fuzzy, kde-format
msgid "Five past five"
msgstr "fimm mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:84
#, fuzzy, kde-format
msgid "Ten past five"
msgstr "tíu mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:85
#, fuzzy, kde-format
msgid "Quarter past five"
msgstr "korter yfir %1"

#: package/contents/ui/FuzzyClock.qml:86
#, fuzzy, kde-format
msgid "Twenty past five"
msgstr "tuttugu mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:87
#, fuzzy, kde-format
msgid "Twenty-five past five"
msgstr "tuttuguogfimm mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:88
#, fuzzy, kde-format
msgid "Half past five"
msgstr "%1 þrjátíu"

#: package/contents/ui/FuzzyClock.qml:89
#, fuzzy, kde-format
msgid "Twenty-five to six"
msgstr "tuttuguogfimm mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:90
#, fuzzy, kde-format
msgid "Twenty to six"
msgstr "tuttugu mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:91
#, fuzzy, kde-format
msgid "Quarter to six"
msgstr "korter í %1"

#: package/contents/ui/FuzzyClock.qml:92
#, fuzzy, kde-format
msgid "Ten to six"
msgstr "tíu mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:93
#, fuzzy, kde-format
msgid "Five to six"
msgstr "fimm mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:94
#, fuzzy, kde-format
msgid "Six o’clock"
msgstr "á slaginu %1"

#: package/contents/ui/FuzzyClock.qml:95
#, fuzzy, kde-format
msgid "Five past six"
msgstr "fimm mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:96
#, fuzzy, kde-format
msgid "Ten past six"
msgstr "tíu mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:97
#, fuzzy, kde-format
msgid "Quarter past six"
msgstr "korter yfir %1"

#: package/contents/ui/FuzzyClock.qml:98
#, fuzzy, kde-format
msgid "Twenty past six"
msgstr "tuttugu mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:99
#, fuzzy, kde-format
msgid "Twenty-five past six"
msgstr "tuttuguogfimm mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:100
#, fuzzy, kde-format
msgid "Half past six"
msgstr "%1 þrjátíu"

#: package/contents/ui/FuzzyClock.qml:101
#, fuzzy, kde-format
msgid "Twenty-five to seven"
msgstr "tuttuguogfimm mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:102
#, fuzzy, kde-format
msgid "Twenty to seven"
msgstr "tuttugu mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:103
#, fuzzy, kde-format
msgid "Quarter to seven"
msgstr "korter í %1"

#: package/contents/ui/FuzzyClock.qml:104
#, kde-format
msgid "Ten to seven"
msgstr ""

#: package/contents/ui/FuzzyClock.qml:105
#, fuzzy, kde-format
msgid "Five to seven"
msgstr "fimm mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:106
#, kde-format
msgid "Seven o’clock"
msgstr ""

#: package/contents/ui/FuzzyClock.qml:107
#, fuzzy, kde-format
msgid "Five past seven"
msgstr "fimm mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:108
#, fuzzy, kde-format
msgid "Ten past seven"
msgstr "tíu mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:109
#, fuzzy, kde-format
msgid "Quarter past seven"
msgstr "korter yfir %1"

#: package/contents/ui/FuzzyClock.qml:110
#, fuzzy, kde-format
msgid "Twenty past seven"
msgstr "tuttugu mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:111
#, fuzzy, kde-format
msgid "Twenty-five past seven"
msgstr "tuttuguogfimm mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:112
#, fuzzy, kde-format
msgid "Half past seven"
msgstr "%1 þrjátíu"

#: package/contents/ui/FuzzyClock.qml:113
#, fuzzy, kde-format
msgid "Twenty-five to eight"
msgstr "tuttuguogfimm mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:114
#, fuzzy, kde-format
msgid "Twenty to eight"
msgstr "tuttugu mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:115
#, fuzzy, kde-format
msgid "Quarter to eight"
msgstr "korter í %1"

#: package/contents/ui/FuzzyClock.qml:116
#, kde-format
msgid "Ten to eight"
msgstr ""

#: package/contents/ui/FuzzyClock.qml:117
#, fuzzy, kde-format
msgid "Five to eight"
msgstr "fimm mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:118
#, kde-format
msgid "Eight o’clock"
msgstr ""

#: package/contents/ui/FuzzyClock.qml:119
#, fuzzy, kde-format
msgid "Five past eight"
msgstr "fimm mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:120
#, fuzzy, kde-format
msgid "Ten past eight"
msgstr "tíu mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:121
#, fuzzy, kde-format
msgid "Quarter past eight"
msgstr "korter yfir %1"

#: package/contents/ui/FuzzyClock.qml:122
#, fuzzy, kde-format
msgid "Twenty past eight"
msgstr "tuttugu mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:123
#, fuzzy, kde-format
msgid "Twenty-five past eight"
msgstr "tuttuguogfimm mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:124
#, fuzzy, kde-format
msgid "Half past eight"
msgstr "%1 þrjátíu"

#: package/contents/ui/FuzzyClock.qml:125
#, fuzzy, kde-format
msgid "Twenty-five to nine"
msgstr "tuttuguogfimm mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:126
#, fuzzy, kde-format
msgid "Twenty to nine"
msgstr "tuttugu mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:127
#, fuzzy, kde-format
msgid "Quarter to nine"
msgstr "korter í %1"

#: package/contents/ui/FuzzyClock.qml:128
#, kde-format
msgid "Ten to nine"
msgstr ""

#: package/contents/ui/FuzzyClock.qml:129
#, fuzzy, kde-format
msgid "Five to nine"
msgstr "fimm mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:130
#, kde-format
msgid "Nine o’clock"
msgstr ""

#: package/contents/ui/FuzzyClock.qml:131
#, fuzzy, kde-format
msgid "Five past nine"
msgstr "fimm mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:132
#, fuzzy, kde-format
msgid "Ten past nine"
msgstr "tíu mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:133
#, fuzzy, kde-format
msgid "Quarter past nine"
msgstr "korter yfir %1"

#: package/contents/ui/FuzzyClock.qml:134
#, fuzzy, kde-format
msgid "Twenty past nine"
msgstr "tuttugu mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:135
#, fuzzy, kde-format
msgid "Twenty-five past nine"
msgstr "tuttuguogfimm mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:136
#, fuzzy, kde-format
msgid "Half past nine"
msgstr "%1 þrjátíu"

#: package/contents/ui/FuzzyClock.qml:137
#, fuzzy, kde-format
msgid "Twenty-five to ten"
msgstr "tuttuguogfimm mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:138
#, fuzzy, kde-format
msgid "Twenty to ten"
msgstr "tuttugu mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:139
#, fuzzy, kde-format
msgid "Quarter to ten"
msgstr "korter í %1"

#: package/contents/ui/FuzzyClock.qml:140
#, fuzzy, kde-format
msgid "Ten to ten"
msgstr "tíu mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:141
#, fuzzy, kde-format
msgid "Five to ten"
msgstr "fimm mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:142
#, fuzzy, kde-format
msgid "Ten o’clock"
msgstr "á slaginu %1"

#: package/contents/ui/FuzzyClock.qml:143
#, fuzzy, kde-format
msgid "Five past ten"
msgstr "fimm mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:144
#, fuzzy, kde-format
msgid "Ten past ten"
msgstr "tíu mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:145
#, fuzzy, kde-format
msgid "Quarter past ten"
msgstr "korter yfir %1"

#: package/contents/ui/FuzzyClock.qml:146
#, fuzzy, kde-format
msgid "Twenty past ten"
msgstr "tuttugu mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:147
#, fuzzy, kde-format
msgid "Twenty-five past ten"
msgstr "tuttuguogfimm mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:148
#, fuzzy, kde-format
msgid "Half past ten"
msgstr "%1 þrjátíu"

#: package/contents/ui/FuzzyClock.qml:149
#, fuzzy, kde-format
msgid "Twenty-five to eleven"
msgstr "tuttuguogfimm mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:150
#, fuzzy, kde-format
msgid "Twenty to eleven"
msgstr "tuttugu mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:151
#, fuzzy, kde-format
msgid "Quarter to eleven"
msgstr "korter í %1"

#: package/contents/ui/FuzzyClock.qml:152
#, fuzzy, kde-format
msgid "Ten to eleven"
msgstr "ellefu"

#: package/contents/ui/FuzzyClock.qml:153
#, kde-format
msgid "Five to eleven"
msgstr ""

#: package/contents/ui/FuzzyClock.qml:154
#, kde-format
msgid "Eleven o’clock"
msgstr ""

#: package/contents/ui/FuzzyClock.qml:155
#, fuzzy, kde-format
msgid "Five past eleven"
msgstr "fimm mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:156
#, fuzzy, kde-format
msgid "Ten past eleven"
msgstr "tíu mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:157
#, fuzzy, kde-format
msgid "Quarter past eleven"
msgstr "korter yfir %1"

#: package/contents/ui/FuzzyClock.qml:158
#, fuzzy, kde-format
msgid "Twenty past eleven"
msgstr "tuttugu mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:159
#, fuzzy, kde-format
msgid "Twenty-five past eleven"
msgstr "tuttuguogfimm mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:160
#, fuzzy, kde-format
msgid "Half past eleven"
msgstr "%1 þrjátíu"

#: package/contents/ui/FuzzyClock.qml:161
#, fuzzy, kde-format
msgid "Twenty-five to twelve"
msgstr "tuttuguogfimm mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:162
#, fuzzy, kde-format
msgid "Twenty to twelve"
msgstr "tuttugu mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:163
#, fuzzy, kde-format
msgid "Quarter to twelve"
msgstr "korter í %1"

#: package/contents/ui/FuzzyClock.qml:164
#, fuzzy, kde-format
msgid "Ten to twelve"
msgstr "tólf"

#: package/contents/ui/FuzzyClock.qml:165
#, kde-format
msgid "Five to twelve"
msgstr ""

#: package/contents/ui/FuzzyClock.qml:166
#, kde-format
msgid "Twelve o’clock"
msgstr ""

#: package/contents/ui/FuzzyClock.qml:167
#, fuzzy, kde-format
msgid "Five past twelve"
msgstr "fimm mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:168
#, fuzzy, kde-format
msgid "Ten past twelve"
msgstr "tíu mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:169
#, fuzzy, kde-format
msgid "Quarter past twelve"
msgstr "korter yfir %1"

#: package/contents/ui/FuzzyClock.qml:170
#, fuzzy, kde-format
msgid "Twenty past twelve"
msgstr "tuttugu mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:171
#, fuzzy, kde-format
msgid "Twenty-five past twelve"
msgstr "tuttuguogfimm mínútur yfir %1"

#: package/contents/ui/FuzzyClock.qml:172
#, fuzzy, kde-format
msgid "Half past twelve"
msgstr "%1 þrjátíu"

#: package/contents/ui/FuzzyClock.qml:173
#, fuzzy, kde-format
msgid "Twenty-five to one"
msgstr "tuttuguogfimm mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:174
#, fuzzy, kde-format
msgid "Twenty to one"
msgstr "tuttugu mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:175
#, fuzzy, kde-format
msgid "Quarter to one"
msgstr "korter í %1"

#: package/contents/ui/FuzzyClock.qml:176
#, fuzzy, kde-format
msgid "Ten to one"
msgstr "tíu mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:177
#, fuzzy, kde-format
msgid "Five to one"
msgstr "fimm mínútur í %1"

#: package/contents/ui/FuzzyClock.qml:181
#, kde-format
msgid "Sleep"
msgstr ""

#: package/contents/ui/FuzzyClock.qml:181
#, kde-format
msgid "Breakfast"
msgstr ""

#: package/contents/ui/FuzzyClock.qml:181
#, kde-format
msgid "Second Breakfast"
msgstr ""

#: package/contents/ui/FuzzyClock.qml:181
#, kde-format
msgid "Elevenses"
msgstr ""

#: package/contents/ui/FuzzyClock.qml:182
#, kde-format
msgid "Lunch"
msgstr ""

#: package/contents/ui/FuzzyClock.qml:182
#, fuzzy, kde-format
msgid "Afternoon tea"
msgstr "Eftirmiðdagur"

#: package/contents/ui/FuzzyClock.qml:182
#, kde-format
msgid "Dinner"
msgstr ""

#: package/contents/ui/FuzzyClock.qml:182
#, kde-format
msgid "Supper"
msgstr ""

#: package/contents/ui/FuzzyClock.qml:186
#, fuzzy, kde-format
msgid "Night"
msgstr "Nótt"

#: package/contents/ui/FuzzyClock.qml:186
#, fuzzy, kde-format
msgid "Early morning"
msgstr "Snemma morguns"

#: package/contents/ui/FuzzyClock.qml:186
#, fuzzy, kde-format
msgid "Morning"
msgstr "Morgunn"

#: package/contents/ui/FuzzyClock.qml:186
#, fuzzy, kde-format
msgid "Almost noon"
msgstr "Fyrir hádegi"

#: package/contents/ui/FuzzyClock.qml:187
#, fuzzy, kde-format
msgid "Noon"
msgstr "Hádegi"

#: package/contents/ui/FuzzyClock.qml:187
#, fuzzy, kde-format
msgid "Afternoon"
msgstr "Eftirmiðdagur"

#: package/contents/ui/FuzzyClock.qml:187
#, fuzzy, kde-format
msgid "Evening"
msgstr "Kvöld"

#: package/contents/ui/FuzzyClock.qml:187
#, fuzzy, kde-format
msgid "Late evening"
msgstr "Síðkvöld"

#: package/contents/ui/FuzzyClock.qml:191
#, fuzzy, kde-format
msgid "Start of week"
msgstr "Upphaf viku"

#: package/contents/ui/FuzzyClock.qml:191
#, fuzzy, kde-format
msgid "Middle of week"
msgstr "Mið vika"

#: package/contents/ui/FuzzyClock.qml:191
#, fuzzy, kde-format
msgid "End of week"
msgstr "Lok viku"

#: package/contents/ui/FuzzyClock.qml:191
#, fuzzy, kde-format
msgid "Weekend!"
msgstr "Helgi!"

#, fuzzy
#~ msgctxt "@title:group"
#~ msgid "Appearance"
#~ msgstr "Útlit"

#, fuzzy
#~ msgctxt "hour in the messages below"
#~ msgid "one"
#~ msgstr "einn"

#, fuzzy
#~ msgctxt "hour in the messages below"
#~ msgid "two"
#~ msgstr "tvistur"

#, fuzzy
#~ msgctxt "hour in the messages below"
#~ msgid "three"
#~ msgstr "þristur"

#, fuzzy
#~ msgctxt "hour in the messages below"
#~ msgid "four"
#~ msgstr "fjarki"

#, fuzzy
#~ msgctxt "hour in the messages below"
#~ msgid "five"
#~ msgstr "fimma"

#, fuzzy
#~ msgctxt "hour in the messages below"
#~ msgid "six"
#~ msgstr "sexa"

#, fuzzy
#~ msgctxt "hour in the messages below"
#~ msgid "seven"
#~ msgstr "sjöa"

#, fuzzy
#~ msgctxt "hour in the messages below"
#~ msgid "eight"
#~ msgstr "átta"

#, fuzzy
#~ msgctxt "hour in the messages below"
#~ msgid "nine"
#~ msgstr "nía"

#, fuzzy
#~ msgctxt "hour in the messages below"
#~ msgid "ten"
#~ msgstr "tía"

#, fuzzy
#~ msgctxt ""
#~ "Whether to uppercase the first letter of completed fuzzy time strings "
#~ "above: translate as 1 if yes, 0 if no."
#~ msgid "1"
#~ msgstr "1"

#, fuzzy
#~ msgid "General"
#~ msgstr "Almennt"

#, fuzzy
#~ msgctxt ""
#~ "@label Short date: %1 day in the month, %2 short month name, %3 year"
#~ msgid "%1 %2 %3"
#~ msgstr "%1 %2 %3"

#, fuzzy
#~ msgctxt "@label Short date: %1 day in the month, %2 short month name"
#~ msgid "%1 %2"
#~ msgstr "%1 %2"

#, fuzzy
#~ msgctxt "@label Day of the week with date: %1 short day name, %2 short date"
#~ msgid "%1, %2"
#~ msgstr "%1, %2"

#, fuzzy
#~ msgid "Check if you want the font in bold"
#~ msgstr "Hakaðu við ef þú vilt feitletur"

#, fuzzy
#~ msgid "When this is checked, the clock font will be bold."
#~ msgstr "Þegar þetta er virkt, verður letrið í klukkunni feitletrað."

#, fuzzy
#~ msgid "&Bold"
#~ msgstr "&Feitletrað"

#, fuzzy
#~ msgid "Check if you want the font in italic"
#~ msgstr "Hakaðu við ef þú vilt skáletur"

#, fuzzy
#~ msgid "When this is checked, the clock font will be in italic."
#~ msgstr "Þegar þetta er virkt, verður letrið í klukkunni skáletrað."

#, fuzzy
#~ msgid "Font color:"
#~ msgstr "Textalitur:"

#, fuzzy
#~ msgid "Use current desktop theme color"
#~ msgstr "Nota liti úr núverandi skjáborðsþema"

#, fuzzy
#~ msgid ""
#~ "This is default. The clock will get its font color from the current "
#~ "desktop theme."
#~ msgstr ""
#~ "Þetta er sjálfgefið. Klukkan mun nota liti úr núverandi skjáborðsþema."

#, fuzzy
#~ msgid "Use theme color"
#~ msgstr "Nota lit úr þema"

#, fuzzy
#~ msgid "Choose your own font color"
#~ msgstr "Veldu þinn eigin lit á letur"

#, fuzzy
#~ msgid ""
#~ "When checked you can choose a custom color for the clock font by clicking "
#~ "on the color widget on the right."
#~ msgstr ""
#~ "Þegar þetta er virkt, geturðu valið sérsniðinn lit á klukkuna með því að "
#~ "smella á litavalsgræjuna hér til hægri."

#, fuzzy
#~ msgid "Use custom color:"
#~ msgstr "Nota sérsniðinn lit:"

#, fuzzy
#~ msgid "Color chooser"
#~ msgstr "Litavalstól"

#, fuzzy
#~ msgid ""
#~ "Click on this button and the KDE standard color dialog will show. You can "
#~ "then choose the new color you want for your clock."
#~ msgstr ""
#~ "Smelltu á þennan hnapp og staðlaða KDE litavalstólið mun birtast. Þú "
#~ "getur þá valið þér lit á klukkuna."

#, fuzzy
#~ msgid "Adjust text to panel-height:"
#~ msgstr "Lagaa texti að hæð:"

#, fuzzy
#~ msgid "0: disable; 2: use full panel-height"
#~ msgstr "0: afvirkja; 2: nota alla hæð spjaldsins"

#, fuzzy
#~ msgid "Information"
#~ msgstr "Upplýsingar"

#, fuzzy
#~ msgid "Show date:"
#~ msgstr "Sýna dagsetningu:"

#, fuzzy
#~ msgid "Display the date of the day"
#~ msgstr "Sýna dagsetningu"

#, fuzzy
#~ msgid "Display day of the week"
#~ msgstr "Sýna vikudag"

#, fuzzy
#~ msgid "Add the day of the week to the date display."
#~ msgstr "Bæta heiti vikudags við dagsetningu"

#, fuzzy
#~ msgid "Show day of the &week"
#~ msgstr "Sýna &vikudag"

#, fuzzy
#~ msgid "Display the current year"
#~ msgstr "Sýna árið sem er núna"

#, fuzzy
#~ msgid "Add the year to the date string."
#~ msgstr "Bæta ártali við dagsetningu."

#, fuzzy
#~ msgid "Show &year"
#~ msgstr "Sýna ár&tal"

#, fuzzy
#~ msgid "Show time zone:"
#~ msgstr "Sýna tímabelti:"

#, fuzzy
#~ msgid "Display the time zone name"
#~ msgstr "Sýna heiti tímabeltis"

#, fuzzy
#~ msgid "Display the time zone name under the time."
#~ msgstr "Sýna tímabelti undir klukkunni."
