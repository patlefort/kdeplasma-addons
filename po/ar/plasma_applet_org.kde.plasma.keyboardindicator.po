# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
#
# Zayed Al-Saidi <zayed.alsaidi@gmail.com>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-19 00:47+0000\n"
"PO-Revision-Date: 2022-06-04 21:01+0400\n"
"Last-Translator: Zayed Al-Saidi <zayed.alsaidi@gmail.com>\n"
"Language-Team: Arabic <kde-l10n-ar@kde.org>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"X-Generator: Lokalize 21.07.70\n"

#: contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Keys"
msgstr "المفاتيح"

#: contents/ui/configAppearance.qml:34
#, kde-format
msgctxt ""
"@label show keyboard indicator when Caps Lock or Num Lock is activated"
msgid "Show when activated:"
msgstr "اعرض عن تنشيط:"

#: contents/ui/configAppearance.qml:37
#, kde-format
msgctxt "@option:check"
msgid "Caps Lock"
msgstr "قفل الحروف الكبيرة"

#: contents/ui/configAppearance.qml:44
#, kde-format
msgctxt "@option:check"
msgid "Num Lock"
msgstr "قفل الأرقام"

#: contents/ui/main.qml:30
#, kde-format
msgid "Caps Lock activated\n"
msgstr "قفل الحروف الكبيرة مفعل\n"

#: contents/ui/main.qml:31
#, kde-format
msgid "Num Lock activated\n"
msgstr "قفل الأرقام مفعل\n"

#: contents/ui/main.qml:111
#, kde-format
msgid "No lock keys activated"
msgstr "لا يوجد قفل مفتاح مفعل"
