msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.plasma.keyboardindicator\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-19 00:47+0000\n"
"PO-Revision-Date: 2021-10-16 18:26+0900\n"
"Last-Translator: R.Suga <21r.suga@gmail.com>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Generator: Poedit 2.4.2\n"

#: contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Keys"
msgstr "キー"

#: contents/ui/configAppearance.qml:34
#, fuzzy, kde-format
#| msgid "No lock keys activated"
msgctxt ""
"@label show keyboard indicator when Caps Lock or Num Lock is activated"
msgid "Show when activated:"
msgstr "有効なロックキーはありません"

#: contents/ui/configAppearance.qml:37
#, kde-format
msgctxt "@option:check"
msgid "Caps Lock"
msgstr "Caps Lock"

#: contents/ui/configAppearance.qml:44
#, kde-format
msgctxt "@option:check"
msgid "Num Lock"
msgstr "Num Lock"

#: contents/ui/main.qml:30
#, kde-format
msgid "Caps Lock activated\n"
msgstr "Caps Lock が有効になりました\n"

#: contents/ui/main.qml:31
#, kde-format
msgid "Num Lock activated\n"
msgstr "Num Lock が有効になりました\n"

#: contents/ui/main.qml:111
#, kde-format
msgid "No lock keys activated"
msgstr "有効なロックキーはありません"
