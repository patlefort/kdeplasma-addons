# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# A S Alam <aalam@users.sf.net>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-25 00:19+0000\n"
"PO-Revision-Date: 2010-01-15 09:16+0530\n"
"Last-Translator: A S Alam <aalam@users.sf.net>\n"
"Language-Team: ਪੰਜਾਬੀ <punjabi-users@lists.sf.net>\n"
"Language: pa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: comic_package.cpp:20
#, kde-format
msgid "Images"
msgstr "ਚਿੱਤਰ"

#: comic_package.cpp:25
#, kde-format
msgid "Executable Scripts"
msgstr "ਚੱਲਣਯੋਗ ਸਕ੍ਰਿਪਟਾਂ"

#: comic_package.cpp:29
#, kde-format
msgid "Main Script File"
msgstr "ਮੇਨ ਸਕ੍ਰਿਪਟ ਫਾਇਲ"
