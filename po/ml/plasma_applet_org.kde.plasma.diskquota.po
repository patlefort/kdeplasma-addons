# Malayalam translations for kdeplasma-addons package.
# Copyright (C) 2019 This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
# Automatically generated, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-07-31 00:48+0000\n"
"PO-Revision-Date: 2018-08-16 09:14+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Swathanthra|സ്വതന്ത്ര Malayalam|മലയാളം Computing|കമ്പ്യൂട്ടിങ്ങ് <smc."
"org.in>\n"
"Language: ml\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: package/contents/ui/main.qml:77
#, kde-format
msgctxt "@info:status"
msgid "No quota restrictions found"
msgstr ""

#: package/contents/ui/main.qml:77
#, kde-format
msgctxt "@info:status"
msgid "Quota tool not found"
msgstr ""

#: package/contents/ui/main.qml:78
#, kde-format
msgctxt "@info:usagetip"
msgid "Please install 'quota'"
msgstr ""

#: plugin/DiskQuota.cpp:44 plugin/DiskQuota.cpp:177 plugin/DiskQuota.cpp:264
#, kde-format
msgid "Disk Quota"
msgstr ""

#: plugin/DiskQuota.cpp:45
#, kde-format
msgid "Please install 'quota'"
msgstr ""

#: plugin/DiskQuota.cpp:178
#, kde-format
msgid "Running quota failed"
msgstr ""

#: plugin/DiskQuota.cpp:239
#, kde-format
msgctxt "usage of quota, e.g.: '/home/bla: 38% used'"
msgid "%1: %2% used"
msgstr ""

#: plugin/DiskQuota.cpp:240
#, kde-format
msgctxt "e.g.: 12 GiB of 20 GiB"
msgid "%1 of %2"
msgstr ""

#: plugin/DiskQuota.cpp:241
#, kde-format
msgctxt "e.g.: 8 GiB free"
msgid "%1 free"
msgstr ""

#: plugin/DiskQuota.cpp:261
#, kde-format
msgctxt "example: Quota: 83% used"
msgid "Quota: %1% used"
msgstr ""

#: plugin/DiskQuota.cpp:265
#, kde-format
msgid "No quota restrictions found."
msgstr ""
