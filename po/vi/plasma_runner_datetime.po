# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
#
# Phu Hung Nguyen <phu.nguyen@kdemail.net>, 2020, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-03-03 00:45+0000\n"
"PO-Revision-Date: 2022-01-10 21:56+0100\n"
"Last-Translator: Phu Hung Nguyen <phu.nguyen@kdemail.net>\n"
"Language-Team: Vietnamese <kde-l10n-vi@kde.org>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 20.12.2\n"

#: datetimerunner.cpp:17
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "date"
msgstr "ngay"

#: datetimerunner.cpp:18
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "time"
msgstr "gio"

#: datetimerunner.cpp:25
#, kde-format
msgid "Displays the current date"
msgstr "Hiển thị ngày hiện tại"

#: datetimerunner.cpp:26
#, kde-format
msgid "Displays the current time"
msgstr "Hiển thị giờ hiện tại"

#: datetimerunner.cpp:27 datetimerunner.cpp:29
#, kde-format
msgctxt "The <> and space are part of the example query"
msgid " <timezone>"
msgstr " <múi giờ>"

#: datetimerunner.cpp:28
#, kde-format
msgid "Displays the current date in a given timezone"
msgstr "Hiển thị ngày hiện tại ở một múi giờ chỉ định"

#: datetimerunner.cpp:30
#, kde-format
msgid "Displays the current time in a given timezone"
msgstr "Hiển thị giờ hiện tại ở một múi giờ chỉ định"

#: datetimerunner.cpp:43
#, kde-format
msgid "Today's date is %1"
msgstr "Ngày hôm nay là %1"

#: datetimerunner.cpp:57
#, kde-format
msgid "Current time is %1"
msgstr "Giờ hiện tại là %1"
