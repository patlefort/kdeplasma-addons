# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Lê Hoàng Phương <herophuong93@gmail.com>, 2012.
# Phu Hung Nguyen <phu.nguyen@kdemail.net>, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-06-30 00:46+0000\n"
"PO-Revision-Date: 2022-05-12 12:12+0200\n"
"Last-Translator: Phu Hung Nguyen <phu.nguyen@kdemail.net>\n"
"Language-Team: Vietnamese <kde-l10n-vi@kde.org>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.12.2\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: package/contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr "Diện mạo"

#: package/contents/ui/configAppearance.qml:30
#, kde-format
msgid "%1pt"
msgstr "%1pt"

#: package/contents/ui/configAppearance.qml:36
#, kde-format
msgid "Text font size:"
msgstr "Cỡ phông chữ:"

#: package/contents/ui/configAppearance.qml:41
#, kde-format
msgid "Background color"
msgstr "Màu nền"

#: package/contents/ui/configAppearance.qml:77
#, kde-format
msgid "A white sticky note"
msgstr "Mảnh giấy nhớ màu trắng"

#: package/contents/ui/configAppearance.qml:78
#, kde-format
msgid "A black sticky note"
msgstr "Mảnh giấy nhớ màu đen"

#: package/contents/ui/configAppearance.qml:79
#, kde-format
msgid "A red sticky note"
msgstr "Mảnh giấy nhớ màu đỏ"

#: package/contents/ui/configAppearance.qml:80
#, kde-format
msgid "An orange sticky note"
msgstr "Mảnh giấy nhớ màu cam"

#: package/contents/ui/configAppearance.qml:81
#, kde-format
msgid "A yellow sticky note"
msgstr "Mảnh giấy nhớ màu vàng"

#: package/contents/ui/configAppearance.qml:82
#, kde-format
msgid "A green sticky note"
msgstr "Mảnh giấy nhớ màu lục"

#: package/contents/ui/configAppearance.qml:83
#, kde-format
msgid "A blue sticky note"
msgstr "Mảnh giấy nhớ màu lam"

#: package/contents/ui/configAppearance.qml:84
#, kde-format
msgid "A pink sticky note"
msgstr "Mảnh giấy nhớ màu hồng"

#: package/contents/ui/configAppearance.qml:85
#, kde-format
msgid "A translucent sticky note"
msgstr "Mảnh giấy nhớ trong mờ"

#: package/contents/ui/configAppearance.qml:86
#, kde-format
msgid "A translucent sticky note with light text"
msgstr "Mảnh giấy nhớ trong mờ với chữ sáng màu"

#: package/contents/ui/main.qml:246
#, kde-format
msgid "Undo"
msgstr "Đảo ngược"

#: package/contents/ui/main.qml:254
#, kde-format
msgid "Redo"
msgstr "Làm lại"

#: package/contents/ui/main.qml:264
#, kde-format
msgid "Cut"
msgstr "Cắt"

#: package/contents/ui/main.qml:272
#, kde-format
msgid "Copy"
msgstr "Chép"

#: package/contents/ui/main.qml:280
#, kde-format
msgid "Paste Without Formatting"
msgstr "Dán không kèm định dạng"

#: package/contents/ui/main.qml:286
#, kde-format
msgid "Paste"
msgstr "Dán"

#: package/contents/ui/main.qml:295
#, kde-format
msgid "Delete"
msgstr "Xoá"

#: package/contents/ui/main.qml:302
#, kde-format
msgid "Clear"
msgstr "Làm trống"

#: package/contents/ui/main.qml:312
#, kde-format
msgid "Select All"
msgstr "Chọn tất cả"

#: package/contents/ui/main.qml:440
#, kde-format
msgctxt "@info:tooltip"
msgid "Bold"
msgstr "Đậm"

#: package/contents/ui/main.qml:452
#, kde-format
msgctxt "@info:tooltip"
msgid "Italic"
msgstr "Nghiêng"

#: package/contents/ui/main.qml:464
#, kde-format
msgctxt "@info:tooltip"
msgid "Underline"
msgstr "Gạch chân"

#: package/contents/ui/main.qml:476
#, kde-format
msgctxt "@info:tooltip"
msgid "Strikethrough"
msgstr "Gạch xuyên"

#: package/contents/ui/main.qml:550
#, kde-format
msgid "Discard this note?"
msgstr "Vứt giấy nhớ?"

#: package/contents/ui/main.qml:551
#, kde-format
msgid "Are you sure you want to discard this note?"
msgstr "Bạn có chắc chắn muốn vứt mảnh giấy nhớ này?"

#: package/contents/ui/main.qml:564
#, kde-format
msgctxt "@item:inmenu"
msgid "White"
msgstr "Trắng"

#: package/contents/ui/main.qml:565
#, kde-format
msgctxt "@item:inmenu"
msgid "Black"
msgstr "Đen"

#: package/contents/ui/main.qml:566
#, kde-format
msgctxt "@item:inmenu"
msgid "Red"
msgstr "Đỏ"

#: package/contents/ui/main.qml:567
#, kde-format
msgctxt "@item:inmenu"
msgid "Orange"
msgstr "Cam"

#: package/contents/ui/main.qml:568
#, kde-format
msgctxt "@item:inmenu"
msgid "Yellow"
msgstr "Vàng"

#: package/contents/ui/main.qml:569
#, kde-format
msgctxt "@item:inmenu"
msgid "Green"
msgstr "Lục"

#: package/contents/ui/main.qml:570
#, kde-format
msgctxt "@item:inmenu"
msgid "Blue"
msgstr "Lam"

#: package/contents/ui/main.qml:571
#, kde-format
msgctxt "@item:inmenu"
msgid "Pink"
msgstr "Hồng"

#: package/contents/ui/main.qml:572
#, kde-format
msgctxt "@item:inmenu"
msgid "Translucent"
msgstr "Trong mờ"

#: package/contents/ui/main.qml:573
#, kde-format
msgctxt "@item:inmenu"
msgid "Translucent Light"
msgstr "Trong mờ sáng"
